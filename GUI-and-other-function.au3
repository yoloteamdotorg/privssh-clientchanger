#include-once
#include <GUIConstantsEx.au3>
#include <GuiListView.au3>

Func _loginArea()
	Local $hLoginGUI = GUICreate("SSH Private Client - LoginArea", 286, 229, -1, -1, -1, 0x00000080)
	GUISetBkColor(0xE5E5E5)
	GUICtrlCreateLabel("Username", 29, 23, 55, 14)
	GUICtrlSetFont(-1, 9, 400, 0, "Tahoma")
	Local $hIUsername = GUICtrlCreateInput($Username, 100, 21, 160, 20)
	GUICtrlSetFont(-1, 9, 400, 0, "Tahoma")
	GUICtrlCreateLabel("Password", 30, 57, 55, 14)
	GUICtrlSetFont(-1, 9, 400, 0, "Tahoma")
	Local $hIPassword = GUICtrlCreateInput($Password, 100, 55, 160, 19, 0x0020)
	GUICtrlSetFont(-1, 9, 400, 0, "Tahoma")
	Local $hCRemember = GUICtrlCreateCheckbox("Remember me", 100, 90, 100, 15)
	If $Username And $Password Then GUICtrlSetState(-1, 1)
	GUICtrlSetFont(-1, 9, 400, 0, "Tahoma")
	Local $hBLogin = GUICtrlCreateButton("Login", 57, 121, 100, 30)
	GUICtrlSetFont(-1, 9, 400, 0, "Tahoma")
	Local $hBSignUp = GUICtrlCreateButton("SignUp", 170, 121, 60, 30)
	GUICtrlSetState(-1, 128)
	GUICtrlSetFont(-1, 9, 400, 0, "Tahoma")
	GUICtrlCreateGroup("", 5, 156, 275, 65)
	GUICtrlCreateLabel("SSH Private Server @ 2017 YoloTEAM", 32, 176, 230, 14, 0x01)
	GUICtrlSetFont(-1, 9, 400, 0, "Tahoma")
	Local $hLink = GUICtrlCreateLabel("https://privssh.yoloteam.org/", 67, 196, 160, 15)
	GUICtrlSetCursor(-1, 0)
	GUICtrlSetColor(-1, 0x0080FF)
	GUICtrlSetFont(-1, 9, 400, 4, "Tahoma")
	GUISetState(@SW_SHOW, $hLoginGUI)
	WinSetOnTop($hLoginGUI, '', 1)

	While 1
		Switch GUIGetMsg($hLoginGUI)
			Case -3
				Exit
			Case $hLink
				ConsoleWrite(1)
				ShellExecute("https://yoloteam.org/category/16/private-ssh-manager")
			Case $hBLogin
				GUICtrlSetState($hBLogin, 128)
				$Username = GUICtrlRead($hIUsername)
				$Password = GUICtrlRead($hIPassword)
				Local $getJWT = _getJWT()
				If $getJWT == 1 Then
					GUISetState(@SW_HIDE, $hLoginGUI)

					If _IsChecked($hCRemember) Then
						IniWrite($configFile, "PrivateSshServer", "Username", $Username)
						IniWrite($configFile, "PrivateSshServer", "Password", $Password)
					Else
						IniWrite($configFile, "PrivateSshServer", "Username", "")
						IniWrite($configFile, "PrivateSshServer", "Password", "")
					EndIf

					GUICtrlSetState($hBLogin, 64)
					GUIDelete($hLoginGUI)
					GUISetState(@SW_SHOW, $hMemberAreaGUI)
					ExitLoop
				Else
					MsgBox(0, "Error", $getJWT, 0, $hLoginGUI)
				EndIf
				GUICtrlSetState($hBLogin, 64)
;~ 			Case $hBSignUp
;~ 				If $sPort < 1 Then
;~ 					ShellExecute("http://" & $sServer & "/SignUp/")
;~ 					ShellExecute("http://" & $sServer)
;~ 				Else
;~ 					ShellExecute("http://" & $sServer & ":" & $sPort & "/SignUp/")
;~ 					ShellExecute("http://" & $sServer & ":" & $sPort)
;~ 				EndIf
		EndSwitch
	WEnd
EndFunc   ;==>_loginArea

Func _SshToArray()
	GUICtrlSetData($hLStatus, ">> Get SSH list from Server ...")
	Local $Sshs = REST_getSocks($GlobalJWT, $SocksRqNumber, $SocksRqCampaign, $SocksRqCountry, $SocksRqState, $SocksRqCity)
	FileDelete(@ScriptDir & '\SavedSsh\get.txt')

	If StringInStr($Sshs, '{"sucess":false') Then
		GUICtrlSetData($hLStatus, ">> " & $Sshs)
		$stop_wait = True
		GUICtrlSetData($hLStatus, ">> Can't get SSH list.")
	Else
		FileWrite(@ScriptDir & '\SavedSsh\get.txt', $Sshs)
		_FileReadToArray(@ScriptDir & '\SavedSsh\get.txt', $tempSsh, 0, "|")
	EndIf
EndFunc   ;==>_SshToArray

Func _changeSsh()
	GUICtrlSetData($hLCurrentSocks, '__')
	Local $BvSsh_loginState, $Get_tempSsh

	While Not $stop_wait
		If UBound($tempSsh) < 1 Then ExitLoop

		$Get_tempSsh = $tempSsh[0][0] & " | " & $tempSsh[0][1] & " | " & $tempSsh[0][2]

		_ArrayDelete($tempSsh, 0)

		$BvSsh_loginState = _GUI_LoginSSH($Get_tempSsh)

		If $BvSsh_loginState > 0 Then
			ExitLoop
		ElseIf $BvSsh_loginState == 0 Then
			GUICtrlSetData($hLStatus, ">> Next SSH ...")
		Else
			ExitLoop
		EndIf

		GUICtrlSetData($hLBL, "--")
		GUICtrlSetColor($hLBL, 0xFFFFFF)

		If UBound($tempSsh) < 2 Then _SshToArray()

		Sleep(500)
	WEnd

	$stop_wait = False
EndFunc   ;==>_changeSsh

Func _GUI_LoginSSH($txt)
	If StringInStr(FileRead(@ScriptDir & '\SaveSsh\live.txt'), $txt) Or StringInStr(FileRead(@ScriptDir & '\SaveSsh\die.txt'), $txt) Then Return 0

	Local $SSH = StringSplit(StringStripWS($txt, 8), "|")
	If $SSH[0] < 3 Then Return
	$BvSsh_loginState = _BvSsh_Login($SSH[1], $SSH[2], $SSH[3])

	If $BvSsh_loginState > 0 Then
		FileWrite(@ScriptDir & '\SavedSsh\live.txt', $txt & @CRLF)
		GUICtrlSetData($hLCurrentSocks, $txt)
		GUICtrlSetData($hLStatus, ">> Using SSH: " & $SSH[1] & " - Forwarding " & $BvSsh_listenInterface & ":" & $BvSsh_listenPort)
		Sleep(1000)
		If $checkBL == 'yes' Then
			GUICtrlSetData($hLBL, "|")
			Local $BL = _dsbl($BvSsh_listenPort)
			If $BL == 0 Then
				GUICtrlSetData($hLBL, "NO")
				GUICtrlSetColor($hLBL, 0x00FF00)
			ElseIf $BL == 1 Then
				GUICtrlSetData($hLBL, "YES")
				GUICtrlSetColor($hLBL, 0xFF80C0)
				If $nextIfBL == 'yes' Then
					Sleep(2000)
					Return 0
				EndIf
			Else
				GUICtrlSetData($hLBL, "N/A")
				GUICtrlSetColor($hLBL, 0xFFFFFF)
			EndIf
		EndIf

		Return 1
	ElseIf $BvSsh_loginState == 0 Then
		GUICtrlSetData($hLStatus, ">> SSH Die ...")
		FileWrite(@ScriptDir & '\SavedSsh\die.txt', $txt & @CRLF)
		Return 0
	Else
		Return $BvSsh_loginState
		GUICtrlSetData($hLStatus, ">> READY .")
	EndIf
EndFunc   ;==>_GUI_LoginSSH

Func _comboxCampaign()
	GUICtrlSetData($hLStatus, ">> Get Campaign list...")
	GUICtrlSetState($hCmbCampaign, 128)
	GUICtrlSetState($hCmbCountry, 128)
	GUICtrlSetState($hCmbState, 128)
	GUICtrlSetState($hCmbCity, 128)

	Local $Campaign = REST_getCampaign($GlobalJWT)

	ConsoleWrite($Campaign & @CRLF)

	Local $sCampagin, $aCampaign[UBound($Campaign) / 2][2]
	For $i = 0 To UBound($Campaign) - 1 Step 2
		$aCampaign[$i / 2][0] = $Campaign[$i]
		$aCampaign[$i / 2][1] = $Campaign[$i + 1] == -1 ? "Unlimited" : $Campaign[$i + 1]
	Next

	_ArraySort($aCampaign)

	For $i = 0 To UBound($aCampaign) - 1
		$sCampagin &= " [" & $aCampaign[$i][0] & "] - " & $aCampaign[$i][1] & "|"
	Next

	GUICtrlSetData($hCmbCampaign, "")
	GUICtrlSetData($hCmbCountry, "")
	GUICtrlSetData($hCmbState, "")
	GUICtrlSetData($hCmbCity, "")
	GUICtrlSetData($hCmbState, " --Select State--", " --Select State--")
	GUICtrlSetData($hCmbCity, " --Select City--", " --Select City--")
	GUICtrlSetData($hCmbCountry, " --Select Country--", " --Select Country--")
	GUICtrlSetData($hCmbCampaign, " --Select Campaign--|" & $sCampagin, " --Select Campaign--")
	GUICtrlSetData($hLStatus, ">> OK. " & UBound($Campaign) / 2 & " Campaign.")

	GUICtrlSetState($hCmbCampaign, 64)
;~ 	GUICtrlSetState($hCmbCountry, 64)
;~ 	GUICtrlSetState($hCmbState, 64)
;~ 	GUICtrlSetState($hCmbCity, 64)
EndFunc   ;==>_comboxCampaign

Func _ListviewSetWidth($hWnd)
	GUICtrlSendMsg($hWnd, $LVM_SETCOLUMNWIDTH, 0, 30)
	GUICtrlSendMsg($hWnd, $LVM_SETCOLUMNWIDTH, 1, 170)
	GUICtrlSendMsg($hWnd, $LVM_SETCOLUMNWIDTH, 2, 35)
	GUICtrlSendMsg($hWnd, $LVM_SETCOLUMNWIDTH, 3, 119)
EndFunc   ;==>_ListviewSetWidth

