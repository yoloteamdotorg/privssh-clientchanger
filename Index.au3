; #INDEX# =======================================================================================================================
; Title .........: PrivateSSHServer - Client v2
; AutoIt Version : 3.3.14.2
; Description ...: Connect to PrivateSSHServer, pull SSH list via REST_API to use with BitviseSSH
; Author(s) .....: WormIt
; Power by ......: YoloTEAM
; Source ........: https://gitlab.com/yoloteamdotorg/privssh-clientchanger
; ===============================================================================================================================

#RequireAdmin

#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=icon.ico
#AutoIt3Wrapper_Outfile=PrivateSshManager-ClientChanger.exe
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Description=PrivateSshManager-ClientChanger
#AutoIt3Wrapper_Res_Fileversion=2.3.0.0
#AutoIt3Wrapper_Res_ProductVersion=2.3.0.0
#AutoIt3Wrapper_Res_LegalCopyright=YoloTEAM @ 2016
#AutoIt3Wrapper_Res_Field=ProductName|PrivateSshManager-ClientChanger
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

; Global Config-section
Global $sServer, $sPort, $Username, $Password, $SocksRqCampaign, $SocksRqCountry, $SocksRqState, $SocksRqCity, $SocksRqNumber
Global $BvSsh_pID, $BvSsh_path, $BvSsh_listenInterface, $BvSsh_listenPort, $BvSsh_timeOut, $BvSsh_hideAll
Global $configFile = @ScriptDir & '\config.ini', $GlobalJWT
Global $tempSsh, $lastSsh, $stop_wait = False
Global $personalStr, $checkBL, $nextIfBL

_loadingConfig()

#include <WindowsConstants.au3>
#include <GUIConstantsEx.au3>
#include <ComboConstants.au3>
#include <Misc.au3>
#include <Array.au3>
#include <File.au3>

_Singleton(@ScriptName)

Opt("TrayMenuMode", 1)

SplashTextOn("", "Loading...", 130, 50, -1, -1, 1, "Tahoma", 13)

Run(@ScriptDir & '\BvSsh\autoBvSsh.exe')
OnAutoItExitRegister("_indexExit")

DirCreate(@ScriptDir & "\SavedSsh")
DirCreate(@ScriptDir & "\SavedSsh\unUSED")

#Region #MainGUI
Global $hMemberAreaGUI = GUICreate("PrivateSshManager v2", 479, 257, -1, -1)
GUISetBkColor(0x008080)

Global $idFileMenu = GUICtrlCreateMenu("&File")
Global $idFileItem_Exit = GUICtrlCreateMenuItem("Exit", $idFileMenu)
Global $idAccountMenu = GUICtrlCreateMenu("&Account")
Global $idAccountItem_Logout = GUICtrlCreateMenuItem("Logout", $idAccountMenu)

GUICtrlCreateGroup("", 0, 205, 480, 35)
Global $hLStatus = GUICtrlCreateLabel(">> READY .", 17, 219, 320, 15)
GUICtrlSetFont(-1, 8.5, 400, 0, "Tahoma")
GUICtrlSetColor(-1, 0xFFFFFF)

Global $hBChane = GUICtrlCreateButton("Change", 220, 118, 120, 50)
GUICtrlSetState(-1, 128)
GUICtrlSetFont(-1, 10, 800, 0, "Tahoma")
Global $hBStop = GUICtrlCreateButton("STOP", 350, 121, 70, 45)
GUICtrlSetFont(-1, 8.5, 400, 0, "Tahoma")
GUICtrlSetState(-1, 128)

GUICtrlCreateLabel("Socks Number", 22, 123, 95, 16)
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetFont(-1, 9, 800, 0, "Tahoma")
Global $hISocksNumber = GUICtrlCreateInput($SocksRqNumber, 121, 121, 60, 20, 0x0001)
GUICtrlSetFont(-1, 10, 400, 0, "Tahoma")
GUICtrlCreateLabel("Forward Port", 22, 152, 85, 15)
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetFont(-1, 9, 800, 0, "Tahoma")
Global $hIPort = GUICtrlCreateInput($BvSsh_listenPort, 121, 149, 60, 20, 0x0001)
GUICtrlSetFont(-1, 10, 400, 0, "Tahoma")
GUICtrlCreateLabel("Timeout (s)", 22, 181, 70, 15)
GUICtrlSetFont(-1, 9, 800, 0, "Tahoma")
GUICtrlSetColor(-1, 0xFFFFFF)
Global $hITimeout = GUICtrlCreateInput($BvSsh_timeOut, 121, 177, 60, 20, 0x0001)
GUICtrlSetFont(-1, 10, 400, 0, "Tahoma")
Global $hCbHideAll = GUICtrlCreateCheckbox("", 235, 181, 15, 15)
GUICtrlSetFont(-1, 8.5, 400, 0)
GUICtrlSetBkColor(-1, 0x008080)
If $BvSsh_hideAll == 'yes' Then GUICtrlSetState(-1, 1)
Global $hLHideAll = GUICtrlCreateLabel("Hide BvSsh", 255, 180, 75, 15)
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetFont(-1, 10, 800, 0, "Tahoma")

GUICtrlCreateLabel("Campaign", 15, 21, 65, 15)
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetFont(-1, 9, 800, 0, "Tahoma", 5)
Global $hCmbCampaign = GUICtrlCreateCombo(" --Select Campaign--", 85, 17, 175, 24, BitOR(0x0003, 0x00200000))
GUICtrlSetFont(-1, 10, 400, 0, "Tahoma")

GUICtrlCreateLabel("Country", 25, 52, 50, 15)
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetFont(-1, 9, 800, 0, "Tahoma")
Global $hCmbCountry = GUICtrlCreateCombo(" --Select Country--", 85, 48, 130, 24, BitOR(0x0003, 0x00200000))
GUICtrlSetFont(-1, 10, 400, 0, "Tahoma")

Global $hLState = GUICtrlCreateLabel("State", 304, 20, 40, 15)
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetFont(-1, 9, 800, 4, "Tahoma")
Global $hCmbState = GUICtrlCreateCombo(" --Select State--", 346, 16, 120, 24, BitOR(0x0003, 0x00200000))
GUICtrlSetState(-1, 128)
GUICtrlSetFont(-1, 10, 400, 0, "Tahoma")
Global $hRState = GUICtrlCreateRadio("", 283, 21, 13, 14)
GUICtrlSetState(-1, 1)
GUICtrlSetBkColor(-1, 0x408080)

Global $hLCity = GUICtrlCreateLabel("City", 305, 52, 30, 15)
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetFont(-1, 9, 800, 4, "Tahoma")
Global $hCmbCity = GUICtrlCreateCombo(" --Select City--", 346, 48, 120, 24, BitOR(0x0003, 0x00200000))
GUICtrlSetState(-1, 128)
GUICtrlSetFont(-1, 10, 400, 0, "Tahoma")
Global $hRCity = GUICtrlCreateRadio("", 283, 53, 13, 14)
GUICtrlSetBkColor(-1, 0x408080)

Global $hCbCheckBL = GUICtrlCreateCheckbox("", 375, 218, 15, 15)
GUICtrlSetBkColor(-1, 0x008080)
If $checkBL == 'yes' Then GUICtrlSetState(-1, 1)

Global $hBPersonal = GUICtrlCreateButton("i", 465, 85, 18, 25)
GUICtrlSetFont(-1, 8.5, 400, 0, "Courier")
Global $hBReload = GUICtrlCreateButton("R", 229, 48, 30, 25)
GUICtrlSetFont(-1, 8.5, 800, 0, "Arial Black")

GUICtrlCreateLabel("Current Socks :", 30, 90, 100, 15)
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetFont(-1, 10, 800, 0, "Tahoma", 5)
Global $hLCurrentSocks = GUICtrlCreateLabel("__", 140, 90, 300, 15, 0x01)
GUICtrlSetColor(-1, 0x80FF00)
GUICtrlSetFont(-1, 10, 800, 0, "Tahoma")

Global $hLCheckBL = GUICtrlCreateLabel("BL:", 397, 218, 20, 15)
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetFont(-1, 9, 400, 4, "Tahoma", 5)
Global $hLBL = GUICtrlCreateLabel("--", 424, 219, 25, 15, 0x01)
GUICtrlSetColor(-1, 0xFFFFFF)
GUICtrlSetFont(-1, 9, 800, 0, "Tahoma", 5)
Global $hCbNextIfBL = GUICtrlCreateCheckbox("", 458, 218, 15, 15)
GUICtrlSetTip(-1, "Next SSH if BL")
GUICtrlSetBkColor(-1, 0x408080)
If $nextIfBL == 'yes' Then GUICtrlSetState(-1, 1)

If _IsChecked($hCbCheckBL) Then
	GUICtrlSetState($hCbNextIfBL, 64)
Else
	GUICtrlSetState($hCbNextIfBL, 128)
EndIf
#EndRegion #MainGUI

#include "GUI-and-other-function.au3"
#include "Bitvise_autoLogin.au3"
#include "REST_api.au3"
#include "JSON.au3"

While ProcessExists("BvSsh.exe")
	ProcessClose("BvSsh.exe")
	Sleep(35)
WEnd

If _getJWT() <> 1 Then _loginArea()

_comboxCampaign()

GUISetState(@SW_SHOW, $hMemberAreaGUI)

If $lastSsh <> "None|None|None" Then
	If MsgBox(4, "Hello", "Login last SSH?", 0, $hMemberAreaGUI) = 6 Then
		_GUI_LoginSSH($lastSsh)
	Else
		IniWrite($configFile, "PrivateSshClient", "LastSsh", "None|None|None")
	EndIf
EndIf

_FileReadToArray(@ScriptDir & '\SavedSsh\get.txt', $tempSsh, 0, "|")
If UBound($tempSsh) > 0 Then GUICtrlSetState($hBChane, 64)

GUIRegisterMsg($WM_COMMAND, "_WM_COMMAND")

While 1
	Switch GUIGetMsg()
		Case -3, $idFileItem_Exit
			Exit
		Case $idAccountItem_Logout
			IniWrite($configFile, "PrivateSshServer", "Password", "")
			$GlobalJWT = ''
			GUISetState(@SW_HIDE, $hMemberAreaGUI)
			_loginArea()
		Case $hLHideAll
			ControlClick($hMemberAreaGUI, '', $hCbHideAll)
		Case $hLCheckBL
			ControlClick($hMemberAreaGUI, '', $hCbCheckBL)
			If _IsChecked($hCbCheckBL) Then
				GUICtrlSetState($hCbNextIfBL, 64)
			Else
				GUICtrlSetState($hCbNextIfBL, 128)
			EndIf
		Case $hCbCheckBL
			If _IsChecked($hCbCheckBL) Then
				GUICtrlSetState($hCbNextIfBL, 64)
			Else
				GUICtrlSetState($hCbNextIfBL, 128)
			EndIf
		Case $hBChane
			_hBChange()
		Case $hBPersonal
			TrayTip("", $personalStr, 5)
		Case $hBReload
			GUICtrlSetState($hBReload, 128)
			_comboxCampaign()
			GUICtrlSetState($hBReload, 64)
		Case $hLState
			GUICtrlSetState($hRState, 1)
			GUICtrlSetState($hRCity, 4)
		Case $hLCity
			GUICtrlSetState($hRState, 4)
			GUICtrlSetState($hRCity, 1)
		Case $hCmbCampaign
			_hCmbCampaign()
		Case $hCmbCountry
			_hCmbCountry()
		Case $hCmbState
			If GUICtrlRead($hCmbState) <> " --Select State--" Then $SocksRqState = GUICtrlRead($hCmbState)
			$SocksRqCity = ''
			GUICtrlSetData($hCmbCity, " --Select City--")
		Case $hCmbCity
			If GUICtrlRead($hCmbCity) <> " --Select City--" Then $SocksRqState = GUICtrlRead($hCmbCity)
			$SocksRqState = ''
			GUICtrlSetData($hCmbState, " --Select State--")
	EndSwitch
WEnd

Func _hBChange()
	Local $selectCountry = StringRegExp(GUICtrlRead($hCmbCountry), '\[(.*?)\]', 1)
	If IsArray($selectCountry) Or UBound($tempSsh) > 0 Then
		GUICtrlSetState($hBChane, 128)
		GUICtrlSetState($hBReload, 128)
		GUICtrlSetState($hCmbCampaign, 128)
		GUICtrlSetState($hCmbCountry, 128)
		GUICtrlSetState($hCmbState, 128)
		GUICtrlSetState($hCmbCity, 128)

		GUICtrlSetState($hBPersonal, 128)
		GUICtrlSetState($hCbHideAll, 128)
		GUICtrlSetState($hCbCheckBL, 128)
		GUICtrlSetState($hCbNextIfBL, 128)

		$SocksRqNumber = GUICtrlRead($hISocksNumber)
		$BvSsh_listenPort = GUICtrlRead($hIPort)
		$BvSsh_timeOut = GUICtrlRead($hITimeout)

		If $BvSsh_listenPort < 1000 Or $BvSsh_timeOut < 1 Or $SocksRqNumber < 1 Then Return
		GUICtrlSetData($hLStatus, ">> Change SSH ...")

		_writeConfig()

		If UBound($tempSsh) < 2 Then _SshToArray()

		GUICtrlSetState($hBStop, 64)

		_changeSsh()

		GUICtrlSetState($hBStop, 128)
		Sleep(1500)
		GUICtrlSetState($hBChane, 64)
		GUICtrlSetState($hBReload, 64)
		GUICtrlSetState($hCmbCampaign, 64)
		GUICtrlSetState($hCmbCountry, 64)
		GUICtrlSetState($hCmbState, 64)
		GUICtrlSetState($hCmbCity, 64)

		GUICtrlSetState($hBPersonal, 64)
		GUICtrlSetState($hCbHideAll, 64)
		GUICtrlSetState($hCbCheckBL, 64)
		GUICtrlSetState($hCbNextIfBL, 64)
	EndIf
EndFunc   ;==>_hBChange

Func _hCmbCampaign()
	Local $selectCampaign = StringRegExp(GUICtrlRead($hCmbCampaign), '\[(.*?)\]', 1)
	If IsArray($selectCampaign) Then
		GUICtrlSetData($hLStatus, ">> Get Country of " & $selectCampaign[0] & " ...")
		GUICtrlSetState($hCmbCountry, 128)
		$SocksRqState = ''
		$SocksRqCity = ''
		$SocksRqCampaign = $selectCampaign[0]
		Local $Country = REST_getCountry($GlobalJWT, $selectCampaign[0])
		If IsArray($Country) Then
			Local $sCountry, $aCountry[UBound($Country) / 2][2]
			For $i = 0 To UBound($Country) - 1 Step 2
				$aCountry[$i / 2][0] = $Country[$i]
				$aCountry[$i / 2][1] = $Country[$i + 1] == -1 ? "Unlimited" : $Country[$i + 1]
			Next

			_ArraySort($aCountry)

			For $i = 0 To UBound($aCountry) - 1
				$sCountry &= " [" & $aCountry[$i][0] & "] - " & $aCountry[$i][1] & "|"
			Next

			GUICtrlSetData($hCmbCountry, "")
			GUICtrlSetData($hCmbCountry, " --Select Country--|" & $sCountry, " --Select Country--")
			GUICtrlSetData($hLStatus, ">> OK. " & UBound($Country) / 2 & " Country.")
		EndIf

		GUICtrlSetState($hCmbCountry, 64)
	Else
		GUICtrlSetData($hCmbState, "")
		GUICtrlSetData($hCmbCity, "")
		GUICtrlSetData($hCmbCountry, "")
		GUICtrlSetData($hCmbCountry, " --Select Country--", " --Select Country--")
		GUICtrlSetData($hCmbState, " --Select State--", " --Select State--")
		GUICtrlSetData($hCmbCity, " --Select City--", " --Select City--")

		GUICtrlSetState($hBChane, 128)
		GUICtrlSetState($hCmbCountry, 128)
		GUICtrlSetState($hCmbState, 128)
		GUICtrlSetState($hCmbCity, 128)
	EndIf
EndFunc   ;==>_hCmbCampaign

Func _hCmbCountry()
	Local $selectCountry = StringRegExp(GUICtrlRead($hCmbCountry), '\[(.*?)\]', 1)
	If IsArray($selectCountry) Then
		$SocksRqState = ''
		$SocksRqCity = ''
		Local $selectState, $selectCity
		Local $strState, $strCity
		GUICtrlSetData($hLStatus, ">> Get State and City of " & $selectCountry[0] & " ...")
		GUICtrlSetState($hCmbCountry, 128)
		GUICtrlSetState($hCmbState, 128)
		GUICtrlSetState($hCmbCity, 128)
		GUICtrlSetState($hBChane, 64)
		$SocksRqCountry = $selectCountry[0]
		Local $JSONParser = REST_getStateNCity($GlobalJWT, $selectCountry[0])
		If IsArray($JSONParser) Then
			$selectState = StringRegExp($JSONParser[0], '"(.*?)"', 3)
			If IsArray($selectState) Then $strState = _ArrayToString($selectState)
			$selectCity = StringRegExp($JSONParser[1], '"(.*?)"', 3)
			If IsArray($selectCity) Then $strCity = _ArrayToString($selectCity)
		EndIf
		GUICtrlSetData($hCmbState, "")
		GUICtrlSetData($hCmbCity, "")
		GUICtrlSetData($hCmbState, " --Select State--|" & $strState, " --Select State--")
		GUICtrlSetData($hCmbCity, " --Select City--|" & $strCity, " --Select City--")
		GUICtrlSetData($hLStatus, ">> OK. " & UBound($selectState) & " State and " & UBound($selectCity) & " City.")
		GUICtrlSetState($hCmbCountry, 64)
		GUICtrlSetState($hCmbState, 64)
		GUICtrlSetState($hCmbCity, 64)
	Else
		GUICtrlSetData($hCmbState, "")
		GUICtrlSetData($hCmbCity, "")
		GUICtrlSetData($hCmbState, " --Select State--", " --Select State--")
		GUICtrlSetData($hCmbCity, " --Select City--", " --Select City--")
		GUICtrlSetState($hBChane, 128)
		GUICtrlSetState($hCmbState, 128)
		GUICtrlSetState($hCmbCity, 128)
	EndIf
EndFunc   ;==>_hCmbCountry

Func _loadingConfig()
	$sServer = IniRead($configFile, "PrivateSshServer", "Server", "localhost")
	$sPort = IniRead($configFile, "PrivateSshServer", "Port", 8080)
	$Username = IniRead($configFile, "PrivateSshServer", "Username", "")
	$Password = IniRead($configFile, "PrivateSshServer", "Password", "")
	$countryISO = IniRead($configFile, "PrivateSshServer", "CountryISO", "")
	$SocksRqNumber = IniRead($configFile, "PrivateSshClient", "SocksRqNumber", 10)
	$lastSsh = IniRead($configFile, "PrivateSshClient", "LastSsh", "None|None|None")
	$checkBL = IniRead($configFile, "PrivateSshClient", "CheckBlacklist", "no")
	$nextIfBL = IniRead($configFile, "PrivateSshClient", "NextIfBL", "no")

	$BvSsh_path = IniRead($configFile, "BvSshConfig", "BvSshPath", @ScriptDir & "\BvSsh")
	If StringLeft($BvSsh_path, 1) == '\' Then $BvSsh_path = @ScriptDir & $BvSsh_path
	If Not FileExists($BvSsh_path & '\BvSsh.exe') Then
		MsgBox(48, 'Exit', 'Bitvise Not found ???!!!' & @CRLF & $BvSsh_path & '\BvSsh.exe')
		Exit
	EndIf

	$BvSsh_listenInterface = IniRead($configFile, "BvSshConfig", "BvSshListenInterface", "127.0.0.1")
	If _ipToHex($BvSsh_listenInterface) == 0 Then $BvSsh_listenInterface = "127.0.0.1"

	$BvSsh_listenPort = IniRead($configFile, "BvSshConfig", "BvSshListenPort", 1080)
	$BvSsh_timeOut = IniRead($configFile, "BvSshConfig", "BvSshTimeout", 10)
	$BvSsh_hideAll = StringLower(IniRead($configFile, "BvSshConfig", "BvSshHideAll", 'true'))
EndFunc   ;==>_loadingConfig

Func _writeConfig()
	If _IsChecked($hCbHideAll) Then
		IniWrite($configFile, "BvSshConfig", "BvSshHideAll", "yes")
		$BvSsh_hideAll = 'yes'
	Else
		IniWrite($configFile, "BvSshConfig", "BvSshHideAll", "no")
		$BvSsh_hideAll = 'no'
	EndIf

	If _IsChecked($hCbCheckBL) Then
		IniWrite($configFile, "PrivateSshClient", "CheckBlacklist", "yes")
		$checkBL = 'yes'
	Else
		IniWrite($configFile, "PrivateSshClient", "CheckBlacklist", "no")
		$checkBL = 'no'
	EndIf

	If _IsChecked($hCbNextIfBL) Then
		IniWrite($configFile, "PrivateSshClient", "NextIfBL", "yes")
		$nextIfBL = 'yes'
	Else
		IniWrite($configFile, "PrivateSshClient", "NextIfBL", "no")
		$nextIfBL = 'no'
	EndIf

	IniWrite($configFile, "PrivateSshClient", "SocksRqNumber", $SocksRqNumber)
	IniWrite($configFile, "BvSshConfig", "BvSshListenPort", $BvSsh_listenPort)
	IniWrite($configFile, "BvSshConfig", "BvSshTimeout", $BvSsh_timeOut)

	If GUICtrlRead($hLCurrentSocks) <> '__' Then IniWrite($configFile, "PrivateSshClient", "lastSsh", GUICtrlRead($hLCurrentSocks))
EndFunc   ;==>_writeConfig

Func _getJWT()
	ConsoleWrite('--> Login to ' & $sServer & ":" & $sPort & @CRLF)
	Local $dataLogin = REST_login($Username, $Password)
	ConsoleWrite($dataLogin & @CRLF)
	If Not StringInStr($dataLogin, "success") Then
		SplashOff()
		MsgBox(48, "Server is Offline ???!!!", "(" & $sServer & ":" & $sPort & ")" & @CRLF & "Can't connect.")
		Exit
	EndIf

	SplashOff()

	Local $regJWT = StringRegExp($dataLogin, '"success":(.*?),".*":"(.*?)"', 3)
	Local $personal = StringRegExp($dataLogin, '"(.*?)":?([^",}]*)', 3)
	$personalStr = "You have " & $personal[3] & " sock(s) left." & @CRLF & @CRLF & "Your account have " & $personal[5] & " hour(s) left."

	If $regJWT[0] == 'true' Then
		ConsoleWrite("+> Login successfully!" & @CRLF)

		TrayTip("", "Welcome, [ " & StringUpper($Username) & " ]" & @CRLF & @CRLF & $personalStr, 5)
		$GlobalJWT = $regJWT[1]
		Return 1
	Else
		Return $regJWT[1]
		ConsoleWrite("! " & $regJWT[1] & @CRLF)
	EndIf
EndFunc   ;==>_getJWT

Func _WM_COMMAND($hWnd, $Msg, $wparam, $lparam)
	If BitAND($wparam, 0x0000FFFF) = $hBStop Then
		GUICtrlSetData($hLStatus, ">> Stopping ...")
		$stop_wait = True
		GUICtrlSetData($hLBL, "--")
		GUICtrlSetColor($hLBL, 0xFFFFFF)
		GUICtrlSetState($hBStop, 128)
	EndIf

	If _WinAPI_HiWord($wparam) = $CBN_DROPDOWN Then
		Switch _WinAPI_LoWord($wparam)
			Case $hCmbState
				GUICtrlSetState($hRState, 1)
				GUICtrlSetState($hRCity, 4)
			Case $hCmbCity
				GUICtrlSetState($hRState, 4)
				GUICtrlSetState($hRCity, 1)
		EndSwitch
	EndIf

	Local Const $STN_DBLCLK = 1
	Local $nID = BitAND($wparam, 0xFFFF)
	Local $nNotifyCode = BitShift($wparam, 16)
	If $nID = $hLBL And $nNotifyCode = $STN_DBLCLK Then
		GUICtrlSetData($hLBL, "--")
		GUICtrlSetColor($hLBL, 0xFFFFFF)
		Sleep(1000)
		GUICtrlSetData($hLBL, "|")
		Local $BL = _dsbl($BvSsh_listenPort)
		If $BL == 0 Then
			GUICtrlSetData($hLBL, "NO")
			GUICtrlSetColor($hLBL, 0x00FF00)
		ElseIf $BL == 1 Then
			GUICtrlSetData($hLBL, "YES")
			GUICtrlSetColor($hLBL, 0xFF80C0)
			If $nextIfBL == 'yes' Then
				Sleep(2000)
				Return 0
			EndIf
		Else
			GUICtrlSetData($hLBL, "N/A")
			GUICtrlSetColor($hLBL, 0xFFFFFF)
		EndIf
	EndIf

	Return $GUI_RUNDEFMSG
EndFunc   ;==>_WM_COMMAND

Func _indexExit()
	_writeConfig()
	ProcessClose("autoBvSsh.exe")
	ProcessClose($BvSsh_pID)
	FileDelete(@ScriptDir & '\SavedSsh\get.txt')
	If UBound($tempSsh) > 1 Then
		For $i = 0 To UBound($tempSsh) - 1
			FileWrite(@ScriptDir & '\SavedSsh\get.txt', $tempSsh[$i][0] & "|" & $tempSsh[$i][1] & "|" & $tempSsh[$i][2] & @CRLF)
		Next
	EndIf
EndFunc   ;==>_indexExit

Func _IsChecked($idControlID)
	Return BitAND(GUICtrlRead($idControlID), $GUI_CHECKED) = $GUI_CHECKED
EndFunc   ;==>_IsChecked
