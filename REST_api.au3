#include-once
#include <WinHttp.au3>
;~ #include <Array.au3>

OnAutoItExitRegister("_closeWinHTTP")

#cs
	; Config-section
	Global $sServer = "localhost"
	Global $sPort = 6969
#ce

Global $hOpen = _WinHttpOpen(), $hConnect
_WinHttpSetTimeouts($hOpen, 0, 30000, 15000, 15000)
If $sPort < 1 Then
	$sPort = 80
	$hConnect = _WinHttpConnect($hOpen, $sServer)
Else
	$hConnect = _WinHttpConnect($hOpen, $sServer, $sPort)
EndIf

#cs
	; Test
	ConsoleWrite(REST_login("james", "here") & @CRLF)
	ConsoleWrite(REST_getSocks("us", 10, "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1Nzc5NDUwYjI5ZGJkMTAzMDAyMTc0YzUiLCJ1c2VybmFtZSI6ImphbWVzIiwicGFzc3dvcmQiOiIkMmEkMTAkZTNjYktrSVdSRlpnTjZvaDRZNUYudVJNQXJwUlN0WFY5Li4yRllza25aVTBUZ3ZYWWVWZzYiLCJfX3YiOjB9.EzWjUusISf0wCrnILl5EsBxmGcJBTwy7Npg5w4Ol9PM") & @CRLF)
	$getStateNCity = REST_getCountry("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1ODVmNDI0OGIxNzcyMzEzZDAwNDY4MmYiLCJ1c2VybmFtZSI6IjEiLCJwYXNzd29yZCI6IiQyYSQxMCRXZ25lOWF0Wk4uYmlNdTNub3FOZjB1MTJsU0VPTkQ3L2lmOXpMaS52L21WMDlqSmVaMWNLYSIsImRheXNMaW1pdCI6MzAsInNvY2tzTGltaXQiOjEwMCwiY3JlYXRlZE9uIjoiMjAxNi0xMi0yNVQwMzo1MTozNi43NjRaIiwiaXNBZG1pbiI6ZmFsc2V9.rETXyBIJhIzvIHPJvrD6_BCGLXpxT1kUSqs6_erg62c")
	$getStateNCity = REST_getCountry("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1ODVmNDI0OGIxNzcyMzEzZDAwNDY4MmYiLCJ1c2VybmFtZSI6IjEiLCJwYXNzd29yZCI6IiQyYSQxMCRXZ25lOWF0Wk4uYmlNdTNub3FOZjB1MTJsU0VPTkQ3L2lmOXpMaS52L21WMDlqSmVaMWNLYSIsImRheXNMaW1pdCI6MzAsInNvY2tzTGltaXQiOjEwMCwiY3JlYXRlZE9uIjoiMjAxNi0xMi0yNVQwMzo1MTozNi43NjRaIiwiaXNBZG1pbiI6ZmFsc2V9.rETXyBIJhIzvIHPJvrD6_BCGLXpxT1kUSqs6_erg62c", "As")
	_ArrayDisplay($getStateNCity)
#ce

Func REST_login($username, $password)
	Return _WinHttpSimpleRequest($hConnect, "POST", "/api/member/login", Default, "username=" & $username & "&password=" & $password)
EndFunc   ;==>REST_login

Func REST_getCampaign($token)
	Local $getCampaign = _WinHttpSimpleRequest($hConnect, "GET", "/api/getSocks/getCampaign", Default, "", "Authorization: JWT " & $token)
	Return StringRegExp($getCampaign, 'campaignName":"(.*?)".*?totalSocks":(.*?),', 3)
EndFunc   ;==>REST_getCampaign

Func REST_getSocks($token, $number, $campaign, $country, $state = '', $city = '')
	ConsoleWrite("number=" & $number & "&campaign=" & $campaign & "&country=" & $country & "&state=" & $state & "&city=" & $city & @CRLF)
	Local $getSocks = _WinHttpSimpleRequest($hConnect, "POST", "/api/getSocks/getCampaignSocks", Default, "number=" & $number & "&campaign=" & $campaign & "&country=" & $country & "&state=" & $state & "&city=" & $city, "Authorization: JWT " & $token & @CRLF & "Content-type: application/x-www-form-urlencoded")
	Local $getSocksEx = StringRegExp($getSocks, '"ip":"(.*?)","user":"(.*?)","pass":"(.*?)"', 3)
	If IsArray($getSocksEx) Then
		If UBound($getSocksEx) == 3 Then Return $getSocksEx[0] & "|" & $getSocksEx[1] & "|" & $getSocksEx[2]

		$getSocks = ""
		For $i = 0 To UBound($getSocksEx) - 1 Step 3
			$getSocks &= $getSocksEx[$i] & "|" & $getSocksEx[$i + 1] & "|" & $getSocksEx[$i + 2] & @CRLF
		Next
		Return StringTrimRight($getSocks, 2)
	ElseIf $getSocks == '[]' Then
		MsgBox(0, 'Error', 'No Socks on DB', 0, $hMemberAreaGUI)
	Else
		MsgBox(0, 'Error', $getSocks, 0, $hMemberAreaGUI)
	EndIf
EndFunc   ;==>REST_getSocks

Func REST_getCountry($token, $campaign)
	Local $getCountry = _WinHttpSimpleRequest($hConnect, "POST", "/api/getSocks/Country", Default, "campaign=" & $campaign, "Authorization: JWT " & $token & @CRLF & "Content-type: application/x-www-form-urlencoded")
	Return StringRegExp($getCountry, '"(.*?)-Socks":"?([^",}]+)', 3)
EndFunc   ;==>REST_getCountry

Func REST_getStateNCity($token, $country)
	Local $getStateNCity = _WinHttpSimpleRequest($hConnect, "POST", "/api/getSocks/StateNCity", Default, "country=" & $country, "Authorization: JWT " & $token & @CRLF & "Content-type: application/x-www-form-urlencoded")
	Return StringRegExp($getStateNCity, '"state":\[(.*?)\],"city":\[(.*?)\]}', 3)
EndFunc   ;==>REST_getStateNCity

Func _dsbl($port)
	Local $hOpen = _WinHttpOpen("", 3, "127.0.0.1:" & $port)
	_WinHttpSetTimeouts($hOpen, 0, 30000, 15000, 15000)
	Local $hConnect = _WinHttpConnect($hOpen, "whoer.net")
	Local $hRequest = _WinHttpOpenRequest($hConnect, "GET", "/", Default, Default, Default, $WINHTTP_FLAG_SECURE)
	_WinHttpAddRequestHeaders($hRequest, "Host: whoer.net")
	_WinHttpAddRequestHeaders($hRequest, "User-Agent: Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19")
	_WinHttpAddRequestHeaders($hRequest, "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
	_WinHttpAddRequestHeaders($hRequest, "Accept-Language: en-US,en;q=0.5")
	_WinHttpAddRequestHeaders($hRequest, "Connection: keep-alive")
	_WinHttpAddRequestHeaders($hRequest, "Upgrade-Insecure-Requests: 1")
	If _WinHttpSendRequest($hRequest, -1, "") Then
		_WinHttpReceiveResponse($hRequest)
		Local $responeData = _WinHttpQueryHeaders($hRequest)
		Local $responeData = _WinHttpSimpleReadData($hRequest, 2)
		_WinHttpCloseHandle($hRequest)
		_WinHttpCloseHandle($hConnect)
		_WinHttpCloseHandle($hOpen)
		$responeData = BinaryToString($responeData, 4)
		Local $dsbl = StringRegExp($responeData, 'spamhaus.org/query/bl\?ip=.*">[\n\r\s]+(.*?)</a>', 1)
		If IsArray($dsbl) Then
			If $dsbl[0] == 'No' Then
				Return 0
			Else
				Return 1
			EndIf
;~ 			ConsoleWrite($dsbl[0] & @CRLF)
		ElseIf StringRegExp($responeData, '<span class="cont">[\r\n\s]+No') Then
			Return 0
;~ 			ConsoleWrite("No" & @CRLF)
		Else
			Return -2
			ConsoleWrite($responeData & @CRLF)
		EndIf
	Else
		Return -1
	EndIf
EndFunc   ;==>_dsbl

Func _closeWinHTTP()
	_WinHttpCloseHandle($hConnect)
	_WinHttpCloseHandle($hOpen)
EndFunc   ;==>_closeWinHTTP
